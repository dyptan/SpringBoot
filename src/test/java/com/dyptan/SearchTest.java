package com.dyptan;

import com.dyptan.model.Filter;
import com.dyptan.service.SearchService;
import org.apache.http.HttpHost;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.search.SearchHit;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static junit.framework.TestCase.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SearchTest {

    @Autowired
    SearchService connector;

    public static Filter getDummyFilter(){
        Filter filter = new Filter();
        filter.setModels("[{\"Mitsubishi\", \"Saab\"}]");
        filter.setPriceFrom(Integer.valueOf("0"));
        filter.setPriceTo(Integer.valueOf("6000"));
        filter.setYearFrom(Short.valueOf("2000"));
        filter.setYearTo(Short.valueOf("2006"));
        filter.setLimit(Short.valueOf("100"));
        filter.setBrands("Dadi");
        return filter;
    }
    private RestHighLevelClient getRestHighLevelClient(){
        RestHighLevelClient client;
        client = new RestHighLevelClient(
                    RestClient.builder(
                            new HttpHost("localhost", 9200, "http")));
        return client;
    }

    @Test
    public void indexElasticWithFilter() {
        String jsonObject = "{\n" +
                "  \"@timestamp\": \"2018-10-20T17:41:05.171Z\",\n" +
                "  \"race_km\": \"69000\",\n" +
                "  \"price_usd\": \"5400\",\n" +
                "  \"message\": \"Модель: Shuttle, Модифікація: Suv , Рік випуску: 2006 , Тип кузова: Позашляховик / Кроссовер, Kолір: Чорний, Вид палива: Бензин, Об'єм двигуна: 22 см³, Коробка передач: Механічна, Пробіг: 69 000 км, Стан машини: Гаражне зберігання, Додаткові опції: Електросклопiдйомники, Бортовий комп'ютер, Підсилювач керма, Мультимедіа: USB, Безпека: ABS, Розмитнена: Так, Ціна: 5 400 $, <br/>Продам свой рамный джип 4*2, Аналог Toyota Land Cruiser Prado <br />\\r\\nв хорошем состоянии.  Задний привод.<br />\\r\\nПобег 69000тысяч, объём 2.2 куб. см. <br />\\r\\nСалон чистый. Комплект дисков(Германия) + резина в подарок! <br />\\r\\nВсе вопросы по телефону 0503249857 <a href=\\\"https://www.olx.ua/uk/obyavlenie/prodam-dadi-shuttle-IDC0QDM.html\\\">https://www.olx.ua/uk/obyavlenie/prodam-dadi-shuttle-IDC0QDM.html</a><img src=\\\"https://apollo-ireland.akamaized.net:443/v1/files/26taztzx5yd12-UA/image;s=94x72\\\">\",\n" +
                "  \"published\": \"2018-10-20T15:29:57.000Z\",\n" +
                "  \"title\": \"Продам Dadi Shuttle\",\n" +
                "  \"year\": \"2006\",\n" +
                "  \"model\": \"Shuttle\",\n" +
                "  \"category\": \"Dadi\",\n" +
                "  \"link\": \"https://www.olx.ua/uk/obyavlenie/prodam-dadi-shuttle-IDC0QDM.html\",\n" +
                "  \"engine_cubic_cm\": \"22\"\n" +
                "}";

        IndexRequest indexRequest = new IndexRequest("olx_test", "cars", "1");
        indexRequest.source(jsonObject, XContentType.JSON);

        IndexResponse response = null;

        try {
            response = getRestHighLevelClient().index(indexRequest);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String id = response.getId();
        String index = response.getIndex();
        String type = response.getType();

        assertEquals("1", id);
        assertEquals("olx_test", index);
        assertEquals("cars", type);

        Filter filter = getDummyFilter();

        List<Map<String, Object>> documents = connector.getHitsAsList(filter);

        for (Map<String, Object> doc:documents
        ) {
            System.out.println(doc.get("model"));
            System.out.println(doc.get("category"));
            assertEquals("Dadi", doc.get("category"));
            assertEquals("Shuttle", doc.get("model"));
        }
    }

    @Test
    public void getAllBrands(){
        Set<String> brands = connector.getBrands();
        for (String brand:brands
             ) {
            System.out.println(brand);
        }
    }

    @Test
    public void getAllData(){
        SearchHit[] data = connector.getAllHits(new SearchRequest("olxua"));
        Arrays.stream(data)
                .map(x->x.getSourceAsString())
                .limit(5)
                .forEach(System.out::println);
    }

    @Test
    public void getFilteredData(){
        SearchHit[] data = connector.getFilteredHits(getDummyFilter());
        Arrays.stream(data)
                .map(x->x.getSourceAsString())
                .limit(5)
                .forEach(System.out::println);
    }

    @Test
    public void getAllSearchHits() {

        SearchResponse response = null;

        try {
            response = getRestHighLevelClient().search(new SearchRequest("olx"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(response.getHits().getHits());
    }
}

